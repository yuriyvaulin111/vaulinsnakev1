// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Snake.generated.h"

class ASnakeElementBase;
class AFood;
class ASpeedBonus;
class ASpeedBonus1;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class VAULINSNAKE_API ASnake : public AActor
{
	GENERATED_BODY()

	FTimerHandle TimerHandle;
	
public:	
	// Sets default values for this actor's properties
	ASnake();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY()
		TArray <ASnakeElementBase*> SnakeElements;
	UPROPERTY()
		EMovementDirection LastMoveDirection;
	UPROPERTY()
		EMovementDirection LastInputDirection;
	UPROPERTY(BlueprintReadOnly)
		int BonusCounter;
	UPROPERTY(EditDefaultsOnly)
		int MaxBonuses = 3;
	UPROPERTY(EditDefaultsOnly)
		float MinX = -420.0;
	UPROPERTY(EditDefaultsOnly)
		float MaxX = 420.0;
	UPROPERTY(EditDefaultsOnly)
		float MaxY = 420.0;
	UPROPERTY(EditDefaultsOnly)
		float MinY = -420.0;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> SnakeFoodClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedBonus> SnakeBonusClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedBonus1> SnakeBonus1Class;
private:
	void SpawnBonus();
	FTransform GenerateRandomLocation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION(BlueprintCallable)
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
		void IncreaseSpeed(const float Coeff);
		void DecreaseSpeed(const float Coeff);
		void SetActorHiddenInGame();
};
