// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "SpeedBonus.h"
#include "SpeedBonus1.h"
// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 50.f;
	LastInputDirection = EMovementDirection::DOWN;
	
}

void ASnake::SpawnBonus()
{
	if (BonusCounter < MaxBonuses)
	{
		BonusCounter++;
		int BonusIndex = FMath::RandRange(0, 2);
		switch (BonusIndex)
		{
		case 0:
			GetWorld()->SpawnActor<AFood>(SnakeFoodClass,
				GenerateRandomLocation());

			break;
		case 1:
			GetWorld()->SpawnActor<ASpeedBonus>(SnakeBonusClass,
				GenerateRandomLocation());

			break;
		default:
			GetWorld()->SpawnActor<ASpeedBonus1>(SnakeBonus1Class,
				GenerateRandomLocation());

			break;

		}
	}
}

FTransform ASnake::GenerateRandomLocation()
{
	const float RandX = FMath::RandRange(MinX, MaxX);
	const float RandY = FMath::RandRange(MinY, MaxY);
	return FTransform(FVector(RandX, RandY, 0));
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
	SetActorTickInterval(MovementSpeed);

	GetWorldTimerManager().SetTimer(TimerHandle, this, &ASnake::SpawnBonus, 5, true);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnake::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, -0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		NewSnakeElem->SetActorHiddenInGame(true);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			NewSnakeElem->SetActorHiddenInGame(false);
		}
	}
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	LastMoveDirection = LastInputDirection;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorLocalOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}

	SnakeElements[0]->AddActorLocalOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnake::IncreaseSpeed(const float Coeff)
{
	MovementSpeed /= Coeff;
	SetActorTickInterval(MovementSpeed);
}

void ASnake::DecreaseSpeed(const float Coeff)
{
	MovementSpeed *= Coeff;
	SetActorTickInterval(MovementSpeed);
}


void ASnake::SetActorHiddenInGame()
{
	bool bNewHidden = false;
}

