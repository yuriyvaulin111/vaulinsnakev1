// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "VaulinSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class VAULINSNAKE_API AVaulinSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
