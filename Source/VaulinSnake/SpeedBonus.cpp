// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus.h"
#include "Snake.h"

// Sets default values
ASpeedBonus::ASpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Coeff = FMath::RandRange(0.5, 2.f);

}

// Called when the game starts or when spawned
void ASpeedBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBonus::Interact(AActor* Interactor, bool bIsHead)
{
	const auto Snake = Cast<ASnake>(Interactor);
	if (IsValid(Snake))
	{
		Snake->IncreaseSpeed(Coeff);
		Snake->BonusCounter--;
		this->Destroy();
	}
}

