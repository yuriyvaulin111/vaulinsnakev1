// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus1.h"
#include "Snake.h"

// Sets default values
ASpeedBonus1::ASpeedBonus1()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Coeff = FMath::RandRange(0.5, 2.f);

}

// Called when the game starts or when spawned
void ASpeedBonus1::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASpeedBonus1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBonus1::Interact(AActor* Interactor, bool bIsHead)
{
	const auto Snake = Cast<ASnake>(Interactor);
	if (IsValid(Snake))
	{
		Snake->DecreaseSpeed(Coeff);
		Snake->BonusCounter--;
		this->Destroy();
	}
}

